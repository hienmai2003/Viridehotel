$(document).ready(function() {
var showMenu = $(".header__control-show");
var hideMenu = $(".header__control-hide");

$(showMenu).click(function() {
        $('.header-nav__list').addClass('show')
        $(showMenu).css('display', 'none');
        $(hideMenu).css('display', 'block');
    });
$(hideMenu).click(function() {
        $('.header-nav__list').removeClass('show');
        $(showMenu).css('display', 'block');
        $(hideMenu).css('display', 'none');
    });
});
$(document).ready(function(){
       $(window).bind('scroll', function() {
             if ($(window).scrollTop() > 50) {
                 $('.header').addClass('header-fixed');
             }
             else {
                 $('.header').removeClass('header-fixed');
             }
        });
    });
AOS.init({
  duration: 1200,
});


